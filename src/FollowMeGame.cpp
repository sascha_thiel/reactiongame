#include "FollowMeGame.h"

void FollowMeGame::init() {
    gameTimer.init(500, false);
    gameTimer.setTimerCallback(this);
}

void FollowMeGame::start() {
    episodeSize = 0;
    points = 0;
    addToneAndStartPlay();
}

void FollowMeGame::addToneAndStartPlay() {
    currentTone = 0;
    episode[episodeSize++] = random(1, 6);
    lcd.clear();
    lcd.home();
    lcd.print("Hoer gut zu!");
    lcd.setCursor(0, 1);
    lcd.print("Punkte: ");
    lcd.print(points);
    gameTimer.setTimerId(0);
    gameTimer.setTime(500);
    gameTimer.start();
    ledRegister.turnOn(episode[currentTone]);
    soundManager.playTone(tones[episode[currentTone] - 1]);
    soundManager.setEndCallback(this);
    currentTone++;
}

void FollowMeGame::stop() {
    gameTimer.stop();
    gameRegister = B00000000;
    soundManager.setEndCallback(nullptr);
}

void FollowMeGame::update(uint16_t diff) {
    Game::update(diff);
    gameTimer.update(diff);
}

void FollowMeGame::onTimerTick(uint8_t timerId, uint8_t tickCount) {
    if (timerId == 1) {
        addToneAndStartPlay();
    } else {
        ledRegister.clearGameLeds();
    }
}

void FollowMeGame::onSoundEnd() {
    if (currentTone >= episodeSize) {
        bitSet(gameRegister, 0);
        ledRegister.clearGameLeds();
        lcd.clear();
        lcd.home();
        lcd.print("Jetzt Du!");
        lcd.setCursor(0, 1);
        lcd.print("Punkte: ");
        lcd.print(points);
        currentTone = 0;
        soundManager.setEndCallback(nullptr);
    } else {
        gameTimer.start();
        ledRegister.turnOn(episode[currentTone]);
        soundManager.playTone(tones[episode[currentTone] - 1]);
        currentTone++;
    }
}

String FollowMeGame::getModeName(uint8_t modeId) {
    return modeId == 0 ? "Normal" : "Endlos";
}

uint8_t FollowMeGame::getCurrentModeId() {
    if (bitRead(gameRegister, 7)) {
        return 0;
    } else if (bitRead(gameRegister, 6)) {
        return 1;
    }
    return 0;
}

uint8_t FollowMeGame::getModeCount() {
    return 2;
}

void FollowMeGame::setMode(uint8_t modeId) {
    if (modeId == 0) {
        bitClear(this->gameRegister, 6);
        bitSet(this->gameRegister, 7);
    } else if (modeId == 1) {
        bitClear(this->gameRegister, 7);
        bitSet(this->gameRegister, 6);
    }
}

void FollowMeGame::onBtnStateChange(uint8_t led, ABtnState state) {
    if (bitRead(gameRegister, 0) && state == ABtnState::PRESSED) {
        if (episode[currentTone] == led) {
            soundManager.playTone(tones[episode[currentTone] - 1]);
            currentTone++;
            if (currentTone >= episodeSize) {
                bitClear(gameRegister, 0);
                gameTimer.setTimerId(1);
                gameTimer.setTime(1500);
                gameTimer.start();
                lcd.setCursor(0, 1);
                lcd.print("Punkte: ");
                lcd.print(points);
                points += 25;
                if (bitRead(gameRegister, 7) && episodeSize >= 5) {
                    stop();
                    if (gameEndedCallback) {
                        gameEndedCallback();
                    }
                }
            }
        } else {
            stop();
            if (gameEndedCallback) {
                gameEndedCallback();
            }
        }
    }
}
