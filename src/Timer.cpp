#include "Timer.h"

void Timer::init(uint16_t time) {
    this->init(time, false);
}

void Timer::init(uint16_t time, bool loop) {
    this->time = time;
    this->tickCount = 0;
    bitWrite(this->timerState, 0, loop);
    bitSet(this->timerState, 2);
}

void Timer::setTimerId(uint8_t timerId) {
    this->timerId = timerId;
}

uint8_t Timer::getTimerId() {
    return this->timerId;
}

void Timer::setTime(uint16_t time) {
    this->time = time;
}

uint16_t Timer::getTime() {
    return this->time;
}

void Timer::setLoop(bool loop) {
    bitWrite(this->timerState, 0, loop);
}

bool Timer::getLoop() {
    return bitRead(this->timerState, 0);
}

void Timer::start() {
    if (bitRead(this->timerState, 2) && !bitRead(this->timerState, 1)) {
        this->elapsedTime = 0;
        bitSet(this->timerState, 1);
    }
}

void Timer::stop() {
    if (bitRead(this->timerState, 1)) {
        this->resetTickCount();
        this->elapsedTime = 0;
        bitClear(this->timerState, 1);
    }
}

uint8_t Timer::getTickCount() {
    return this->tickCount;
}

void Timer::resetTickCount() {
    this->tickCount = 0;
}

void Timer::onTick(void (*func)(uint8_t tickCount)) {
    this->tick = func;
}

void Timer::setTimerCallback(TimerCallback *callback) {
    this->timerCallback = callback;
}

void Timer::update(uint16_t diff) {
    if (bitRead(this->timerState, 1)) {
        this->elapsedTime += diff;

        if (this->elapsedTime >= this->time) {
            this->tickCount++;
            if (bitRead(this->timerState, 0)) {
                this->elapsedTime = 0;
            } else {
                this->stop();
            }
            if (this->tick) {
                this->tick(this->tickCount);
            }
            if (this->timerCallback) {
                this->timerCallback->onTimerTick(this->timerId, this->tickCount);
            }
        }
    }
}