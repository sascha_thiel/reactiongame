#include "ReactionGame.h"

void ReactionGame::init() {
    this->gameTimer.init(this->actionTime, true);
    this->gameTimer.setTimerCallback(this);
}

void ReactionGame::start() {
    bitSet(this->gameRegister, 1);
    this->elapsedTime = 0;
    this->points = 0;
    this->round = 0;
    this->stepCounter = 0;

    if (bitRead(this->gameRegister, 7)) {
        this->actionTime = 1000;
        this->pauseTime = 1000;
    } else if (bitRead(this->gameRegister, 6)) {
        this->actionTime = 1500;
        this->pauseTime = 1500;
    }

    this->updateLcd();
    this->setAction();
}

uint8_t ReactionGame::getModeCount() {
    return 2;
}

uint8_t ReactionGame::getCurrentModeId() {
    if (bitRead(this->gameRegister, 7)) {
        return 0;
    } else if (bitRead(this->gameRegister, 6)) {
        return 1;
    }
    return 0;
}

String ReactionGame::getModeName(uint8_t modeId) {
    return modeId == 0 ? "Normal" : "Endlos";
}

void ReactionGame::setMode(uint8_t modeId) {
    if (modeId == 0) {
        bitClear(this->gameRegister, 6);
        bitSet(this->gameRegister, 7);
    } else if (modeId == 1) {
        bitClear(this->gameRegister, 7);
        bitSet(this->gameRegister, 6);
    }
}

void ReactionGame::onTimerTick(uint8_t timerId, uint8_t tickCount) {
    this->gameTimer.stop();

    if (bitRead(this->gameRegister, 0)) {
        // action state end
        if (bitRead(this->gameRegister, 7)) {
            this->setPause();
        } else if (bitRead(this->gameRegister, 6)) {
            this->stop();
            if (this->gameEndedCallback) {
                this->gameEndedCallback();
            }
        }
    } else {
        // pause state end
        if (this->round >= 5 && bitRead(this->gameRegister, 7)) {
            this->stop();
            if (this->gameEndedCallback) {
                this->gameEndedCallback();
            }
        } else {
            this->setAction();
        }
    }
}

void ReactionGame::setAction() {
    this->stepCounter++;
    if (this->stepCounter >= this->stepsPerRound) {
        this->round++;
        this->stepCounter = 0;
        this->updateLcd();
        if (bitRead(this->gameRegister, 6)) {
            this->actionTime -= 50;
            this->pauseTime -= 50;
        }
    }
    bitSet(this->gameRegister, 0);
    uint8_t randNum = random(1, 6);
    this->ledRegister.turnOn(randNum);
    this->gameTimer.setTime(this->actionTime);
    this->gameTimer.start();
}

void ReactionGame::setPause() {
    bitClear(this->gameRegister, 0);
    this->ledRegister.clearGameLeds();
    this->gameTimer.setTime(this->pauseTime);
    this->gameTimer.start();
}

void ReactionGame::update(uint16_t diff) {
    Game::update(diff);
    this->gameTimer.update(diff);
}

void ReactionGame::updateLcd() {
    this->lcd.clear();
    this->lcd.home();
    this->lcd.print("Runde: ");
    this->lcd.print(this->round + 1);
    this->lcd.setCursor(0, 1);
    this->lcd.print("Punkte: ");
    this->lcd.print(this->points);
}

void ReactionGame::stop() {
    this->gameTimer.stop();
    ledRegister.clearGameLeds();
    bitClear(this->gameRegister, 1);
    bitClear(this->gameRegister, 0);
}

void ReactionGame::onBtnStateChange(uint8_t led, ABtnState state) {
    if (!bitRead(this->gameRegister, 1) || !bitRead(this->gameRegister, 0)) {
        return;
    }

    if (state == ABtnState::PRESSED && ledRegister.isOn(led)) {
        this->points += 10;
        this->gameTimer.stop();
        this->soundManager.playMelody(this->hitMelody, 2);

        this->updateLcd();

        this->setPause();

        return;
    }

    this->soundManager.playMelody(this->wrongMelody, 2);
}