#include "LedRegister.h"

LedRegister::LedRegister(uint8_t latchPin, uint8_t dataPin, uint8_t clockPin) {
    this->reg = B00000000;
    this->stateRegister = B10000000;
    this->latchPin = latchPin;
    this->dataPin = dataPin;
    this->clockPin = clockPin;
}

void LedRegister::begin() {
    pinMode(this->latchPin, OUTPUT);
    pinMode(this->dataPin, OUTPUT);
    pinMode(this->clockPin, OUTPUT);

    this->flush();
}

void LedRegister::turnOn(uint8_t led) {
    uint8_t pin = this->getPinFromNumber(led);
    bitSet(this->reg, pin);
    bitSet(this->stateRegister, 7);
}

void LedRegister::turnOff(uint8_t led) {
    uint8_t pin = this->getPinFromNumber(led);
    bitClear(this->reg, pin);
    bitSet(this->stateRegister, 7);
}

void LedRegister::toggle(uint8_t led) {
    uint8_t pin = this->getPinFromNumber(led);
    bitWrite(this->reg, pin, (bitRead(this->reg, pin) ? 0 : 1));
    bitSet(this->stateRegister, 7);
}

bool LedRegister::isOn(uint8_t led) {
    return bitRead(this->reg, this->getPinFromNumber(led));
}

uint8_t LedRegister::getPinFromNumber(uint8_t num) {
    if (num == 0) {
        return 1;
    } else {
        return 7 - num;
    }
}

void LedRegister::flush() {
    if (bitRead(this->stateRegister, 7)) {
        digitalWrite(this->latchPin, LOW);
        shiftOut(this->dataPin, this->clockPin, LSBFIRST, this->reg);
        digitalWrite(this->latchPin, HIGH);
        bitClear(this->stateRegister, 7);
    }
}

void LedRegister::clearGameLeds() {
    this->reg &= B00000010;
    bitSet(this->stateRegister, 7);
}

void LedRegister::clearAll() {
    this->reg = B00000000;
    bitSet(this->stateRegister, 7);
}