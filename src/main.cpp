#include <Arduino.h>
#include <FollowMeGame.h>
#include "LiquidCrystal.h"
#include "ABtn.h"
#include "Game.h"
#include "ReactionGame.h"
#include "LedRegister.h"
#include "Timer.h"
#include "SoundManager.h"
#include "Menu.h"

#define LATCH_PIN 3
#define CLOCK_PIN 4
#define DATA_PIN 2

#define BUZZER_PIN 5

#define START_BTN_PIN A0
#define BTN_1_PIN A5
#define BTN_2_PIN A4
#define BTN_3_PIN A3
#define BTN_4_PIN A2
#define BTN_5_PIN A1

enum class MainState {
    MENU,
    IDLE,
    START,
    GAME_RUN,
    GAME_END
};

// header
void startGame();
void startIdle();
void onStartBtnStateChanged(ABtnState);
void onBtn1StateChanged(ABtnState);
void onBtn2StateChanged(ABtnState);
void onBtn3StateChanged(ABtnState);
void onBtn4StateChanged(ABtnState);
void onBtn5StateChanged(ABtnState);
void toggleIdleText();
void onIdleTimerTick(uint8_t);
void onStartTimerTick(uint8_t);
void onGameEndTimerTick(uint8_t);
void onMenuEnter();
void onMenuExit();
void onGameChange(uint8_t gameId);
void onGameModeChange(uint8_t modeId);
void onGameEnded();
void setup();
void loop();

ABtn startBtn = ABtn(START_BTN_PIN, ABtnMode::EXTERNAL_PULLUP);
ABtn btn1 = ABtn(BTN_1_PIN, ABtnMode::EXTERNAL_PULLUP);
ABtn btn2 = ABtn(BTN_2_PIN, ABtnMode::EXTERNAL_PULLUP);
ABtn btn3 = ABtn(BTN_3_PIN, ABtnMode::EXTERNAL_PULLUP);
ABtn btn4 = ABtn(BTN_4_PIN, ABtnMode::EXTERNAL_PULLUP);
ABtn btn5 = ABtn(BTN_5_PIN, ABtnMode::EXTERNAL_PULLUP);

LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
SoundManager soundManager = SoundManager();

Tone countdownTone1 = (Tone) {NOTE_C5, 200, 0};
Tone countdownTone2 = (Tone) {NOTE_C6, 500, 0};

Tone idleMelody[10] = {
        {NOTE_C4, 300, 150},
        {NOTE_E5, 100, 250},
        {NOTE_E5, 100, 550},
        {NOTE_E5, 100, 250},
        {NOTE_E5, 100, 100},
        {NOTE_C4, 300, 150},
        {NOTE_D5, 100, 250},
        {NOTE_D5, 100, 550},
        {NOTE_D5, 100, 250},
        {NOTE_D5, 100, 5000}};

Tone gameEndMelody[6] = {
        {NOTE_C4, 150, 50},
        {NOTE_F4, 150, 50},
        {NOTE_A4, 150, 50},
        {NOTE_C5, 150, 50},
        {NOTE_B4, 150, 50},
        {NOTE_C5, 350, 0}};

LedRegister ledRegister = LedRegister(LATCH_PIN, DATA_PIN, CLOCK_PIN);
Game *game;
Menu menu = Menu(lcd);

MainState mainState = MainState::IDLE;

unsigned long lastMillis;

// Bit 0 > idle help shown
byte mainRegister = B00000000;

// main second timer
Timer mainTimer = Timer();

void startIdle() {
    mainTimer.stop();

    mainTimer.setTime(1000);
    mainTimer.onTick(onIdleTimerTick);
    onIdleTimerTick(mainTimer.getTickCount());

    lcd.begin(16, 2);
    toggleIdleText();

    soundManager.playMelody(idleMelody, 10, true);

    mainState = MainState::IDLE;
    mainTimer.start();
}

void startGame() {
    soundManager.stop();
    mainTimer.stop();
    mainTimer.setTime(1000);
    mainTimer.onTick(onStartTimerTick);

    lcd.clear();
    lcd.home();
    lcd.print("Spiel Start in:");
    lcd.setCursor(0, 1);
    lcd.print("!!!  3  !!!");

    ledRegister.clearAll();
    ledRegister.turnOn(1);
    ledRegister.turnOn(5);

    soundManager.playTone(countdownTone1);

    mainState = MainState::START;
    mainTimer.start();
}

void onMenuEnter() {
    soundManager.stop();
    mainTimer.stop();

    ledRegister.clearAll();

    mainState = MainState::MENU;
}

void onMenuExit() {
    startIdle();
}

void onGameChange(uint8_t gameId) {
    if (game->getId() == gameId) {
        return;
    }
    game->onGameEnded(nullptr);
    delete game;
    switch (gameId) {
        case 2:
            game = new FollowMeGame(lcd, ledRegister, soundManager);
            break;
        case 1:
        default:
            game = new ReactionGame(lcd, ledRegister, soundManager);
            break;
    }
    game->init();
    game->onGameEnded(onGameEnded);
    menu.setGame(game);
}

void onGameModeChange(uint8_t modeId) {
    if (game->getCurrentModeId() == modeId) {
        return;
    }
    game->setMode(modeId);
}

void onStartBtnStateChanged(ABtnState state) {
    if (mainState == MainState::IDLE || mainState == MainState::MENU || mainState == MainState::GAME_END) {
        menu.onBtnStateChange(0, state);
    }

    // start game if button was released to enter menu if long hold
    if (state == ABtnState::RELEASED && (mainState == MainState::IDLE || mainState == MainState::GAME_END)) {
        startGame();
    }
}

void onBtn1StateChanged(ABtnState state) {
    if (mainState == MainState::MENU) {
        menu.onBtnStateChange(1, state);
    }
    if (mainState == MainState::GAME_RUN) {
        game->onBtnStateChange(1, state);
    }
}

void onBtn2StateChanged(ABtnState state) {
    if (mainState == MainState::MENU) {
        menu.onBtnStateChange(2, state);
    }

    if (mainState == MainState::GAME_RUN) {
        game->onBtnStateChange(2, state);
    }
}

void onBtn3StateChanged(ABtnState state) {
    if (mainState == MainState::MENU) {
        menu.onBtnStateChange(3, state);
    }

    if (mainState == MainState::GAME_RUN) {
        game->onBtnStateChange(3, state);
    }
}

void onBtn4StateChanged(ABtnState state) {
    if (mainState == MainState::MENU) {
        menu.onBtnStateChange(4, state);
    }
    if (mainState == MainState::GAME_RUN) {
        game->onBtnStateChange(4, state);
    }
}

void onBtn5StateChanged(ABtnState state) {
    if (mainState == MainState::MENU) {
        menu.onBtnStateChange(5, state);
    }
    if (mainState == MainState::GAME_RUN) {
        game->onBtnStateChange(5, state);
    }
}

void toggleIdleText() {
    lcd.clear();
    lcd.home();
    if (bitRead(mainRegister, 0)) {
        lcd.print(game->getName());
        lcd.setCursor(0, 1);
        lcd.print("> ");
        lcd.print(game->getModeName(game->getCurrentModeId()));
        bitClear(mainRegister, 0);
    } else {
        lcd.print("Spiel Start");
        lcd.setCursor(0, 1);
        lcd.print("> Start Dr\xF5"
                  "cken");
        bitSet(mainRegister, 0);
    }
}

void onIdleTimerTick(uint8_t tickCount) {
    ledRegister.clearGameLeds();

    if (tickCount % 3 == 0) {
        toggleIdleText();
    }

    if (tickCount % 2) {
        ledRegister.turnOn(1);
        ledRegister.turnOn(3);
        ledRegister.turnOn(5);
        ledRegister.turnOn(0);
    } else {
        ledRegister.turnOn(2);
        ledRegister.turnOn(4);
        ledRegister.turnOff(0);
    }
}

void onStartTimerTick(uint8_t tickCount) {
    if (tickCount <= 3) {
        uint8_t countdown = 3 - tickCount;
        lcd.setCursor(0, 1);
        if (countdown != 0) {
            lcd.print("!!!  ");
            lcd.print(countdown);
            lcd.print("  !!!");
            soundManager.playTone(countdownTone1);
        } else {
            soundManager.playTone(countdownTone2);
            ledRegister.clearGameLeds();
            lcd.print("!!! LOS !!!");
        }

        ledRegister.clearGameLeds();
        if (tickCount == 1) {
            ledRegister.turnOn(2);
            ledRegister.turnOn(4);
        }
        if (tickCount == 2) {
            ledRegister.turnOn(3);
        }
    } else {
        ledRegister.clearAll();
        mainState = MainState::GAME_RUN;
        mainTimer.stop();
        game->start();
    }
}

void onGameEndTimerTick(uint8_t tickCount) {
    startIdle();
}

void onGameEnded() {
    soundManager.playMelody(gameEndMelody, 6);

    mainState = MainState::GAME_END;
    lcd.clear();
    lcd.home();
    lcd.print("Spielende:");
    lcd.setCursor(0, 1);
    lcd.print("> ");
    lcd.print(game->getPoints());
    lcd.print(" < Punkte");

    mainTimer.setTime(15000);
    mainTimer.onTick(onGameEndTimerTick);
    mainTimer.start();
}

void setup() {
//     Serial.begin(9600);
//     Serial.println("Alexander Gamestudio");

    soundManager.init(BUZZER_PIN);

    startBtn.begin();
    btn1.begin();
    btn2.begin();
    btn3.begin();
    btn4.begin();
    btn5.begin();

    startBtn.onStateChange(onStartBtnStateChanged);
    btn1.onStateChange(onBtn1StateChanged);
    btn2.onStateChange(onBtn2StateChanged);
    btn3.onStateChange(onBtn3StateChanged);
    btn4.onStateChange(onBtn4StateChanged);
    btn5.onStateChange(onBtn5StateChanged);

    ledRegister.begin();

    menu.onEnter(onMenuEnter);
    menu.onExit(onMenuExit);
    menu.onGameChange(onGameChange);
    menu.onModeChange(onGameModeChange);
    mainTimer.init(1000, true);

    game = new ReactionGame(lcd, ledRegister, soundManager);
//    game = new FollowMeGame(lcd, ledRegister, soundManager);
    game->init();
    game->onGameEnded(onGameEnded);

    menu.init();
    menu.setGame(game);

    randomSeed(analogRead(A6));
    startIdle();
}

void loop() {
    unsigned long currentMillis = millis();
    uint16_t diff = currentMillis - lastMillis;

    startBtn.loop(diff);
    btn1.loop(diff);
    btn2.loop(diff);
    btn3.loop(diff);
    btn4.loop(diff);
    btn5.loop(diff);

    if (mainState == MainState::IDLE || mainState == MainState::MENU || mainState == MainState::GAME_END) {
        menu.update(diff);
    }

    if (mainState != MainState::GAME_RUN) {
        mainTimer.update(diff);
    }

    if (mainState == MainState::GAME_RUN) {
        game->update(diff);
    }

    soundManager.update(diff);

    ledRegister.flush();

    lastMillis = currentMillis;
}