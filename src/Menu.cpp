#include "Menu.h"

void Menu::init() {
    bitSet(menuRegister, 0);
    timer.init(3000, false);
    timer.setTimerCallback(this);
}

void Menu::setGame(Game *game) {
    this->game = game;
}

void Menu::onEnter(void (*onEnterFunc)()) {
    this->onEnterFunc = onEnterFunc;
}

void Menu::onExit(void (*onExitFunc)()) {
    this->onExitFunc = onExitFunc;
}

void Menu::onGameChange(void (*onGameChangeFunc)(uint8_t gameId)) {
    this->onGameChangeFunc = onGameChangeFunc;
}

void Menu::onModeChange(void (*onModeChangeFunc)(uint8_t modeId)) {
    this->onModeChangeFunc = onModeChangeFunc;
}

void Menu::showMainMenu() {
    lcd.clear();
    lcd.home();
    lcd.print("2 > Spiel");
    lcd.setCursor(0, 1);
    lcd.print("3 > Modus");

    menuSection = 0;
}

void Menu::showGameSelection() {
    lcd.clear();
    lcd.home();
    lcd.print("2 > Raktionsspiel");
    lcd.setCursor(0, 1);
    lcd.print("3 > Folge Mir");

    menuSection = 1;
}

void Menu::showModeSelection() {
    lcd.clear();
    lcd.home();

    if (game->getId() == 1) {
        lcd.print("2 > Normal");
        lcd.setCursor(0, 1);
        lcd.print("3 > Endlos");
    } else if (game->getId() == 2) {
        lcd.print("2 > Normal");
        lcd.setCursor(0, 1);
        lcd.print("3 > Endlos");
    }

    menuSection = 2;
}

void Menu::exitMenu() {
    if (!bitRead(menuRegister, 1)) {
        return;
    }
    bitClear(menuRegister, 1);
    if (onExitFunc) {
        onExitFunc();
    }
}

void Menu::onBtnStateChange(uint8_t btn, ABtnState state) {
    // enter menu
    if (btn == 0 && state == ABtnState::PRESSED) {
        timer.start();
    } else if (btn == 0 && state == ABtnState::RELEASED && !bitRead(menuRegister, 1)) {
        // cancel menu entry timer
        timer.stop();
    }

    // previous menu page
    if (btn == 1 && state == ABtnState::PRESSED) {
        // not implemented yet
    }

    if (btn == 2 && state == ABtnState::PRESSED) {
        if (menuSection == 0) {
            showGameSelection();
        } else if (menuSection == 1) {
            changeGame(1);
            showMainMenu();
        } else if (menuSection == 2) {
            changeMode(1);
            showMainMenu();
        }
    }

    if (btn == 3 && state == ABtnState::PRESSED) {
        if (menuSection == 0) {
            showModeSelection();
        } else if (menuSection == 1) {
            changeGame(2);
            showMainMenu();
        } else if (menuSection == 2) {
            changeMode(2);
            showMainMenu();
        }
    }

    // next menu page
    if (btn == 4 && state == ABtnState::PRESSED) {
        // not implemented yet
    }

    // exit menu
    if (btn == 5 && state == ABtnState::PRESSED) {
        if (menuSection == 0) {
            exitMenu();
        } else {
            showMainMenu();
        }
    }

}

void Menu::changeGame(uint8_t gameId) {
    if (onGameChangeFunc) {
        onGameChangeFunc(gameId);
    }
}

void Menu::changeMode(uint8_t modeId) {
    if (onModeChangeFunc) {
        onModeChangeFunc(1);
    }
}

void Menu::onTimerTick(uint8_t timerId, uint8_t tickCount) {
    if (!bitRead(menuRegister, 1)) {
        bitSet(menuRegister, 1);
        if (onEnterFunc) {
            onEnterFunc();
        }
        showMainMenu();
    }
}

void Menu::update(uint16_t diff) {
    if (!bitRead(menuRegister, 0)) {
        return;
    }
    timer.update(diff);
}


