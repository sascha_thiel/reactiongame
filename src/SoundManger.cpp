#include "SoundManager.h"

void SoundManager::init(uint8_t pin) {
    this->pin = pin;
    this->timer.init(1000, true);
    this->timer.setTimerId(99);
    this->timer.setTimerCallback(this);
    bitSet(this->soundRegister, 0);
}

void SoundManager::playMelody(Tone melodyToPlay[], uint8_t size) {
    this->playMelody(melodyToPlay, size, false);
}

void SoundManager::playMelody(Tone melodyToPlay[], uint8_t size, bool loop) {
    if (!bitRead(this->soundRegister, 0)) {
        return;
    }
    if (loop) {
        bitSet(this->soundRegister, 3);
    }

    bitSet(this->soundRegister, 2);
    this->melody = melodyToPlay;
    this->melodySize = size;
    this->currentMelodyTone = 0;
    this->playTone(this->melody[this->currentMelodyTone]);
}

void SoundManager::playTone(Tone &toneToPlay) {
    if (!bitRead(this->soundRegister, 0)) {
        return;
    }

    this->currentTone = toneToPlay;
    bitSet(this->soundRegister, 1);
    bitSet(this->soundRegister, 7);

    tone(this->pin, this->currentTone.note, this->currentTone.playTime);
    this->timer.setTime(this->currentTone.playTime - 20);
    this->timer.start();
}

void SoundManager::stop() {
    if (bitRead(this->soundRegister, 1)) {
        bitClear(this->soundRegister, 1);
        bitClear(this->soundRegister, 2);
        bitClear(this->soundRegister, 3);
        this->timer.stop();
    }
}

void SoundManager::update(uint16_t diff) {
    if (bitRead(this->soundRegister, 0)) {
        this->timer.update(diff);
    }
}

void SoundManager::onTimerTick(uint8_t timerId, uint8_t tickCount) {
    // if playTime ended just start pause time of the tone
    if (bitRead(this->soundRegister, 7)) {
        bitClear(this->soundRegister, 7);
        this->timer.setTime(this->currentTone.pauseTime);
    }
        // if pauseTime ended
    else {
        // and it is a melody, start the next tone if available
        if (bitRead(this->soundRegister, 2)) {
            this->currentMelodyTone++;
            if (this->currentMelodyTone < this->melodySize) {
                this->playTone(this->melody[this->currentMelodyTone]);
                return;
            } else if (bitRead(this->soundRegister, 3)) {
                this->currentMelodyTone = 0;
                this->playTone(this->melody[this->currentMelodyTone]);
                return;
            }
        }

        this->stop();

        if (this->on_end) {
            this->on_end();
        }
        if (this->endCallback) {
            this->endCallback->onSoundEnd();
        }
    }
}

void SoundManager::onEnd(void (*on_end)()) {
    this->on_end = on_end;
}

void SoundManager::setEndCallback(SoundEndCallback *callback) {
    this->endCallback = callback;
}
