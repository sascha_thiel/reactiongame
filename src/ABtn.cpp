#include "ABtn.h"

ABtn::ABtn(uint8_t pin, ABtnMode mode) {
    this->pin = pin;
    // currently we only have pullup mode implemented
    bitSet(this->btnRegister, 2);
}

void ABtn::begin() {
    if (bitRead(this->btnRegister, 2)) {
        pinMode(this->pin, INPUT);
    }
    bitSet(this->btnRegister, 0);
}

void ABtn::loop(uint16_t diff) {
    if (!bitRead(this->btnRegister, 0)) {
        return;
    }
    int value = digitalRead(this->pin);
    if (bitRead(this->btnRegister, 2)) {
        // turn the signal because of pullup mode
        value = value == HIGH ? LOW : HIGH;
    }

    // state would change
    if (bitRead(this->btnRegister, 4) != value) {
        this->timeElapsed += diff;
    } else {
        this->timeElapsed = 0;
    }

    if (value == LOW && bitRead(this->btnRegister, 4) && this->timeElapsed >= 40) {
        bitClear(this->btnRegister, 4);
        if (!this->state_change) {
            return;
        }
        this->state_change(ABtnState::RELEASED);
    } else if (value == HIGH && !bitRead(this->btnRegister, 4) && this->timeElapsed >= 40) {
        bitSet(this->btnRegister, 4);
        if (!this->state_change) {
            return;
        }
        this->state_change(ABtnState::PRESSED);
    }
}

void ABtn::onStateChange(void (*function)(ABtnState)) {
    this->state_change = function;
}