#ifndef SOUND_END_CALLBACK_H
#define SOUND_END_CALLBACK_H

#include <Arduino.h>

class SoundEndCallback {
public:
    virtual void onSoundEnd() = 0;
};

#endif