#ifndef FOLLOW_ME_GAME_H
#define FOLLOW_ME_GAME_H

#include <Arduino.h>
#include "Game.h"
#include "Timer.h"

class FollowMeGame : public Game, public TimerCallback, public SoundEndCallback {

public:
    FollowMeGame(LiquidCrystal &lcd, LedRegister &ledRegister, SoundManager &soundManager) : Game(lcd, ledRegister,
                                                                                                  soundManager) {};
    ~FollowMeGame() {};

    String getName() { return "Folge Mir!"; }

    uint8_t getId() { return 2; }

    void init() override;
    void start() override;
    void update(uint16_t diff) override;
    void stop() override;
    String getModeName(uint8_t modeId) override;
    uint8_t getCurrentModeId() override;
    uint8_t getModeCount() override;
    void setMode(uint8_t modeId) override;
    void onBtnStateChange(uint8_t led, ABtnState state) override;

private:
    // Bit 0 > 0 computer, 1 player
    // Bit 6 > endless mode
    // Bit 7 > normal mode
    byte gameRegister = B10000000;
    Timer gameTimer = Timer();
    Tone tones[5] = {
            {NOTE_C4, 500, 500},
            {NOTE_D4, 500, 500},
            {NOTE_E4, 500, 500},
            {NOTE_F4, 500, 500},
            {NOTE_G4, 500, 500}
    };
    uint8_t currentTone = 0;
    uint8_t episodeSize = 0;
    uint8_t episode[50];

    Tone* getTone(uint8_t led);
    void addToneAndStartPlay();
    void onTimerTick(uint8_t timerId, uint8_t tickCount);
    void onSoundEnd();
};

#endif