#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include <Arduino.h>
#include "pitches.h"
#include "Timer.h"
#include "TimerCallback.h"
#include "SoundEndCallback.h"

struct Tone {
    int note;
    uint16_t playTime;
    uint16_t pauseTime;
};

class SoundManager : public TimerCallback {
public:
    void init(uint8_t pin);
    void playMelody(Tone melody[], uint8_t melodySize);
    void playMelody(Tone melody[], uint8_t melodySize, bool loop);
    void playTone(Tone &tone);
    void stop();
    void update(uint16_t diff);
    void onEnd(void (*on_end)());
    void setEndCallback(SoundEndCallback *);

private:
    // Bit 0 > sound manager initialized
    // Bit 1 > is playing
    // Bit 2 > 1 is melody 0 is tone
    // Bit 3 > loop mode
    // Bit 7 > 1 is playTime 0 is pauseTime
    byte soundRegister = B00000000;
    uint8_t pin;
    Timer timer = Timer();
    Tone currentTone;
    uint8_t currentMelodyTone;
    uint8_t melodySize;
    Tone *melody;
    void (*on_end)();
    SoundEndCallback *endCallback = nullptr;

    void onTimerTick(uint8_t timerId, uint8_t tickCount);
};

#endif