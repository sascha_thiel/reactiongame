#ifndef GAME_H
#define GAME_H

#include <Arduino.h>
#include "ABtn.h"
#include "LiquidCrystal.h"
#include "LedRegister.h"
#include "SoundManager.h"

class Game {
public:
    Game(LiquidCrystal &lcd, LedRegister &ledRegister, SoundManager &soundManager) : lcd(lcd),
                                                                                     ledRegister(ledRegister),
                                                                                     soundManager(soundManager) {};

    virtual ~Game() {};

    virtual String getName() = 0;
    virtual uint8_t getId() = 0;
    virtual void init() = 0;
    virtual void start() = 0;

    virtual void update(uint16_t diff) {
        elapsedTime += diff;
    }

    virtual void stop() {};

    virtual uint16_t getPoints() {
        return this->points;
    }

    void onGameEnded(void (*callback)()) {
        this->gameEndedCallback = callback;
    }

    virtual String getModeName(uint8_t modeId) = 0;
    virtual uint8_t getCurrentModeId() = 0;
    virtual uint8_t getModeCount() = 0;
    virtual void setMode(uint8_t modeId) = 0;
    virtual void onBtnStateChange(uint8_t led, ABtnState state) = 0;

protected:
    uint8_t round = 0;
    uint16_t elapsedTime = 0;
    uint16_t points = 0;
    void (*gameEndedCallback)();
    LiquidCrystal &lcd;
    LedRegister &ledRegister;
    SoundManager &soundManager;
};

#endif