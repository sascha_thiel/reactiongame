#ifndef LED_REGISTER_H
#define LED_REGISTER_H

#include <Arduino.h>

class LedRegister {
public:
    LedRegister(uint8_t latchPin, uint8_t dataPin, uint8_t clockPin);
    void begin();
    void turnOn(uint8_t led);
    void turnOff(uint8_t led);
    void toggle(uint8_t led);
    bool isOn(uint8_t led);
    void flush();
    void clearGameLeds();
    void clearAll();

private:
    // 10000000 > reg has changed
    byte stateRegister;
    byte reg;
    uint8_t latchPin;
    uint8_t dataPin;
    uint8_t clockPin;

    uint8_t getPinFromNumber(uint8_t num);
};

#endif