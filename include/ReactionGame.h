#ifndef REACTION_GAME_H
#define REACTION_GAME_H

#include <Arduino.h>
#include "Game.h"
#include "Timer.h"
#include "TimerCallback.h"

class ReactionGame : public Game, public TimerCallback {
public:
    ReactionGame(LiquidCrystal &lcd, LedRegister &ledRegister, SoundManager &soundManager) : Game(lcd, ledRegister,
                                                                                                  soundManager) {};
    ~ReactionGame() {};

    String getName() { return "Reaktionsspiel"; };

    uint8_t getId() { return 1; }

    void init() override;
    void start() override;
    void update(uint16_t diff) override;
    void stop() override;

    uint8_t getModeCount() override;
    uint8_t getCurrentModeId() override;
    String getModeName(uint8_t modeId) override;
    void setMode(uint8_t modeId) override;

    void onBtnStateChange(uint8_t led, ABtnState state);

private:
    // Bit 0 > game state 1 action / 0 pause
    // Bit 1 > game started
    // Bit 6 > endless mode
    // Bit 7 > normal mode
    byte gameRegister = B10000000;
    uint16_t actionTime = 1000;
    uint16_t pauseTime = 1000;
    uint8_t stepsPerRound = 5;
    uint8_t stepCounter = 0;
    Timer gameTimer = Timer();
    Tone hitMelody[2] = {
            {NOTE_C4, 100, 0},
            {NOTE_C6, 50,  0}};
    Tone wrongMelody[2] = {
            {NOTE_C5, 100, 0},
            {NOTE_C4, 50,  0}};

    void setAction();
    void setPause();
    void onTimerTick(uint8_t timerId, uint8_t tickCount);
    void updateLcd();
};

#endif