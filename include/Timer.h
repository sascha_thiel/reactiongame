#ifndef TIMER_H
#define TIMER_H

#include <Arduino.h>
#include "TimerCallback.h"

class Timer {
public:
    void init(uint16_t time);
    void init(uint16_t time, bool loop);
    void setTimerId(uint8_t timerId);
    uint8_t getTimerId();
    void setTime(uint16_t time);
    uint16_t getTime();
    void setLoop(bool loop);
    bool getLoop();
    void start();
    void stop();
    uint8_t getTickCount();
    void resetTickCount();
    void onTick(void (*tick)(uint8_t tickCount));
    void setTimerCallback(TimerCallback *);
    void update(uint16_t diff);

private:
    // B00000001 > loop
    // B00000010 > timer is running
    // B00000100 > timeer is initialized
    byte timerState;
    uint8_t timerId = 0;
    uint16_t time;
    uint16_t elapsedTime;
    uint8_t tickCount;
    void (*tick)(uint8_t tickCount);
    TimerCallback *timerCallback = nullptr;
};

#endif