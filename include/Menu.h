#ifndef MENU_H
#define MENU_H

#include <Arduino.h>
#include <LiquidCrystal.h>
#include "Timer.h"
#include "Game.h"
#include "TimerCallback.h"

class Menu : public TimerCallback {
public:
    Menu(LiquidCrystal &lcd) : lcd(lcd) {};
    void init();
    void update(uint16_t diff);
    void setGame(Game *game);

    void onEnter(void (*onEnterFunc)());
    void onExit(void (*onExitFunc)());
    void onGameChange(void (*onGameChangeFunc)(uint8_t gameId));
    void onModeChange(void (*onModeChangeFunc)(uint8_t modeId));

    void onBtnStateChange(uint8_t led, ABtnState state);
private:
    // Bit 0 > menu has initialized
    // Bit 1 > 1 menu entered, 0 menu exit
    // Bit 2 > has more than 2 options
    byte menuRegister = B00000000;
    // 0 > Main section
    // 1 > Game selection
    // 2 > Mode selection
    uint8_t menuSection;
    LiquidCrystal &lcd;
    Timer timer = Timer();
    Game *game;
    void (*onEnterFunc)();
    void (*onExitFunc)();
    void (*onGameChangeFunc)(uint8_t gameId);
    void (*onModeChangeFunc)(uint8_t modeId);

    void exitMenu();
    void showMainMenu();
    void showGameSelection();
    void showModeSelection();
    void changeGame(uint8_t gameId);
    void changeMode(uint8_t modeId);

    void onTimerTick(uint8_t timerId, uint8_t tickCount) override;
};

#endif
