#ifndef ABSTRACT_CALLBACK_H
#define ABSTRACT_CALLBACK_H

#include <Arduino.h>

class TimerCallback {
public:
    virtual void onTimerTick(uint8_t timerId, uint8_t tickCount) = 0;
};

#endif