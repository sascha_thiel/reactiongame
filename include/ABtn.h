#ifndef A_BTN_H
#define A_BTN_H

#include <Arduino.h>
#include "ABtn.h"
#include "LedRegister.h"

enum ABtnMode {
    EXTERNAL_PULLUP
};
enum ABtnState {
    PRESSED,
    RELEASED
};

class ABtn {
public:
    ABtn(uint8_t pin, ABtnMode mode);
    void begin();
    void loop(uint16_t diff);
    void onStateChange(void (*)(ABtnState));

private:
    // B00000001 btn has initialized
    // B00000100 external pullup mode
    // B00010000 btn state 1 pressed 0 released
    byte btnRegister;
    uint16_t timeElapsed = 0;
    uint8_t pin;
    void (*state_change)(ABtnState);
};

#endif